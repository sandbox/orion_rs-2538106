<?php

function rs_orion_form_system_theme_settings_alter(&$form, &$form_state)
{
// adds vertical tabs
    $form['rs_orion'] = array(
        '#type' => 'vertical_tabs',
        '#prefix' => '<h2><small>' . t('Orion Settings') . '</small></h2>',
        '#weight' => -10,
    );

// base seeton tab ----------------
  
    $form['base'] = array(
        '#type' => 'fieldset',
        '#title' => t('Base Settings'),
        '#group' => 'rs_orion',
    );



// base setting -----------------

    $form['base']['base_setting']['basic_desc'] = array(
        '#markup' => t('<img src="' . file_create_url(drupal_get_path('theme', 'rs_orion') . '/images/branding.png') . '" />
         <br><br><strong>Important Note:<br> To display some elements(like sticky menu, parallax etc) the theme requires module jQuery update. <a href="https://www.drupal.org/project/jquery_update" target="_blank">Install it from here</a>and activate.</strong>

        '),
      );

    $form['base']['base_setting']['page_layouts'] = array(
        '#type' => 'select',
        '#title' => t('Page layouts'),
        '#description' => t(''),
        '#default_value' => theme_get_setting('page_layouts'),
        '#description'   => t("<strong>Normal</strong> - full width header, parallax, footer, but fixed 1130px containers.<br> <strong>Boxed</strong> - fixed boxed layouts(default 1130px, to change use css customizer.<br> <strong>Fluid</strong> - no containers, full width with paddind left and right 20px"),
        '#options' => array(
            '0' => t('Normal'),
            '1' => t('Boxed'),
            '2' => t('Full width'),
        ),

    );

    $form['base']['base_setting']['contant_container'] = array(
        '#type' => 'checkbox',
        '#title' => t('Wrap main content'),
        '#default_value' => theme_get_setting('contant_container'),
        '#description'   => t("Check to wrap content and sidebar into container. It's useful if you customize normal layouts dark or colorful background, but want to have main frame will be light(set background color in customizer, default is white. Uncheck if not used."),
    );


// Header setting -----------------

    $form['base']['header_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Header Setting'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['base']['header_settings']['classic_style'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use classic header'),
        '#default_value' => theme_get_setting('classic_style'),
        '#description'   => t("See docs to know what difference is."),
    );

    $form['base']['header_settings']['rs_orion_navbar_position'] = array(
        '#type' => 'select',
        '#title' => t('Sticky Navbar'),
        '#description' => t('Stick your Navbar. The navbar remains at the top of the viewport when scrolling down the site. <br />
        <strong>Note:</strong> To use dropdown menu, parent items should be expanded(check - show as expanded) Use <a href="https://www.drupal.org/project/menu_expanded" target="_blank">module Menu Expanded</a> for quick check.'),
        '#default_value' => theme_get_setting('rs_orion_navbar_position'),
        '#options' => array(
            'sticky' => t('Stick  navbar when it touch the top'),
            'sticky2' => t('Stick with animation at the top but only after 100px scrolling'),
        ),
        '#empty_option' => t('Normal - no sticky'),

    );
    $form['base']['header_settings']['menu_float'] = array(
        '#type' => 'select',
        '#title' => t('Menu position'),
        '#description' => t(''),
        '#default_value' => theme_get_setting('menu_float'),
        '#options' => array(
            'menu-left' => t('Left'),
            'menu-right' => t('Right'),
            'menu-center' => t('Center'),
        ),

    );
    $form['base']['header_settings']['header_height'] = array(
        '#type' => 'textfield',
        '#title' => 'Header height',
        '#default_value' => theme_get_setting('header_height'),
        '#size' => 12,
        '#maxlength' => 8,
        '#required' => FALSE,
        '#description'   => t("Enter the height of your header. Change it, if use your own logo has another height to center items. Default logo height is 60px"),
    );

// Search -----------------

    $form['base']['header_settings']['search_display'] = array(
        '#type' => 'select',
        '#title' => t('Show Search in Header'),
        '#description' => t(''),
        '#default_value' => theme_get_setting('search_display'),
        '#description' => 'You must set permissions for module as well <a href="' . file_create_url(drupal_get_path('root', 'root') . 'admin/people/permissions') . '"  target="_blank"/>here.</a>',

        '#options' => array(
            'show' => t('Show with Button'),
            'hide' => t('Hide'),
        ),

    );



 // Social icons -----------------
    $form['base']['social_contacts'] = array(
        '#type' => 'fieldset',
        '#title' => t('Social Display'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['base']['social_contacts']['site_contacts'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show Social Icon and contact'),
        '#default_value' => theme_get_setting('site_contacts'),
        '#description'   => t("Check this option to show Social Icons. UNCHECK TO HIDE CONTACTS AND TOP DIV."),
    );
    $form['base']['social_contacts']['facebook_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Facebook Profile URL'),
        '#default_value' => theme_get_setting('facebook_url', 'rs_orion'),
        '#description'   => t("Enter your Facebook Profile URL. Leave blank to hide."),
    );
    $form['base']['social_contacts']['vk_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Vkontakte Profile URL'),
        '#default_value' => theme_get_setting('vk_url', 'rs_orion'),
        '#description'   => t("Enter your Vkontakte Profile URL. Leave blank to hide."),
    );
    $form['base']['social_contacts']['twitter_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Twitter Profile URL'),
        '#default_value' => theme_get_setting('twitter_url', 'rs_orion'),
        '#description'   => t("Enter your Twitter Profile URL. Leave blank to hide."),
    );
    $form['base']['social_contacts']['google_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Google+ Profile URL'),
        '#default_value' => theme_get_setting('google_url', 'rs_orion'),
        '#description'   => t("Enter your Google+ Profile URL. Leave blank to hide."),
    );

// Contact info -----------------

    $form['base']['contacts'] = array(
        '#type' => 'fieldset',
        '#title' => t('Contacts'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );

    $form['base']['social_contacts']['site_contacts'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show Social Icon and contact'),
        '#default_value' => theme_get_setting('site_contacts'),
        '#description'   => t("Check this option to show Social Icons. UNCHECK TO HIDE CONTACTS AND TOP DIV."),
    );

    $form['base']['contacts']['company_phone'] = array(
        '#type' => 'textfield',
        '#title' => 'Telephone',
        '#default_value' => theme_get_setting('company_phone'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description'   => t("Enter your Phone. Leave blank to hide."),
    );


    $form['base']['contacts']['company_email'] = array(
        '#type' => 'textfield',
        '#title' => 'Email',
        '#default_value' => theme_get_setting('company_email'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description'   => t("Enter your Email. Leave blank to hide."),
    );





// parallax tab -----------------
    $form['parallax_setting'] = array(
        '#type' => 'fieldset',
        '#title' => t('Parallax Settings'),
        '#group' => 'rs_orion',
    );





// parallax top-----------------
    $form['parallax_setting']['parallax_top'] = array(
        '#type' => 'fieldset',
        '#title' => t('Top Parallax'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['parallax_setting']['parallax_top']['display_parallax_setting'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display parallax'),
        '#default_value' => theme_get_setting('display_parallax_setting'),
        '#description'   => t("Check to display. Uncheck to hide."),
    );
    $form['parallax_setting']['parallax_top']['parallax_text'] = array(

        '#markup' => t('Replace default parallax image which is in <strong>YOURSITE/sites/all/themes/rs_orion/images/parallax-bg.jpg</strong> Or upload your image somewhere and put absolute url below.  Recomended size of image is 1600x600pixels. '),
    );
    $form['parallax_setting']['parallax_top']['parallax_bg_url'] = array(
        '#type' => 'textfield',
        '#title' => 'Background image url',
        '#default_value' => theme_get_setting('parallax_bg_url'),
        '#description'   => t("Enter Background image url. Leave blank to use default."),
    );

    $form['parallax_setting']['parallax_top']['head_parallax_setting'] = array(
        '#type' => 'textfield',
        '#title' => 'Parallax Heading',
        '#default_value' => theme_get_setting('head_parallax_setting'),
    );



    $form['parallax_setting']['parallax_top']['desc_parallax_setting'] = array(
        '#type' => 'textfield',
        '#title' => 'Parallax Description',
        '#size' => 60,
        '#maxlength' => 256,
        '#required' => FALSE,
        '#default_value' => theme_get_setting('desc_parallax_setting'),
    );
    $form['parallax_setting']['parallax_top']['url_parallax_setting'] = array(
        '#type' => 'textfield',
        '#title' => 'Parallax Link',
        '#default_value' => theme_get_setting('url_parallax_setting'),
        '#description'   => t("Enter url. Keep empty, if no link"),
    );

    $form['parallax_setting']['parallax_top']['wrap_text_parallax_setting'] = array(
        '#type' => 'checkbox',
        '#title' => t('Wrap text'),
        '#default_value' => theme_get_setting('wrap_text_parallax_setting'),
        '#description'   => t("Check to wrap your text into half transparant background div. Recommended if your text not good readable. For example if you use light background picture."),
    );

    $form['parallax_setting']['parallax_top']['display_button_parallax_setting'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display read more button'),
        '#default_value' => theme_get_setting('display_button_parallax_setting'),
        '#description'   => t("Check to display. Uncheck to hide."),
    );
    $form['parallax_setting']['parallax_top']['read_more_parallax_setting'] = array(
        '#type' => 'textfield',
        '#title' => 'Read more',
        '#default_value' => theme_get_setting('read_more_parallax_setting'),
        '#description'   => t("Enter text"),
    );



// Layouts tab -----------------

    $form['layouts'] = array(
        '#type' => 'fieldset',
        '#title' => t('Layouts'),
        '#group' => 'rs_orion',
    );

// Slideshow -----------------

    $form['layouts']['slideshow'] = array(
        '#type' => 'fieldset',
        '#title' => t('Slideshow'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['layouts']['slideshow']['slideshow_note'] = array(
        '#type' => 'item',
        '#markup' => t('<strong>Note:</strong>Uncheck Parallax Display to use your own slideshow in this region, or they will be displayed both'),
    );
    $form['layouts']['slideshow']['show_slideshow'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show  Slideshow region'),
        '#default_value' => theme_get_setting('show_slideshow'),
        '#description'   => t("Check this option to show  Slideshow region. Uncheck to hide."),
    );
    $form['layouts']['slideshow']['show_slideshow_container'] = array(
        '#type' => 'checkbox',
        '#title' => t('Wrap  Slideshow into container'),
        '#default_value' => theme_get_setting('show_slideshow_container'),
        '#description'   => t("Check this option to 'Wrap  Slideshow into container.  Container makes fixed width of Slideshow. If to leave uncheck , slideshow remains fluid"),
    );

// Sidebar -----------------

    $form['layouts']['sidebar'] = array(
        '#type' => 'fieldset',
        '#title' => t('Sidebar'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );

    $form['layouts']['sidebar']['rs_orion_sidebar_position'] = array(
        '#type' => 'select',
        '#title' => t('Sidebar Position'),
        '#description' => t('Select position.'),
        '#default_value' => theme_get_setting('rs_orion_sidebar_position'),
        '#options' => array(
            'sidebar_right' => t('Sidebar Right'),
            'sidebar_left' => t('Sidebar Left'),
        ),

    );

    $form['layouts']['sidebar']['sidebar_1_style'] = array(
        '#type' => 'select',
        '#title' => t('Sidebar 1 Panel Style'),
        '#default_value' => theme_get_setting('sidebar_1_style'),
        '#options' => array(
            'uk-panel uk-panel-box' => t('Box'),
            'uk-panel uk-panel-box uk-panel-box-primary' => t('Box Primiry'),
            'uk-panel uk-panel-box uk-panel-box-secondary' => t('Box Secondary'),
            'uk-panel uk-panel-blank' => t('Box Blank'),
        ),

    );

    $form['layouts']['sidebar']['sidebar_2_style'] = array(
        '#type' => 'select',
        '#title' => t('Sidebar 2 Panel Style'),
        '#default_value' => theme_get_setting('sidebar_2_style'),
        '#options' => array(
            'uk-panel uk-panel-box' => t('Box'),
            'uk-panel uk-panel-box uk-panel-box-primary' => t('Box Primiry'),
            'uk-panel uk-panel-box uk-panel-box-secondary' => t('Box Secondary'),
            'uk-panel uk-panel-blank' => t('Box Blank'),
        ),

    );


// Page Top-----------------
    $form['layouts']['page_boxes'] = array(
        '#type' => 'fieldset',
        '#title' => t(' Panels'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );

    $form['layouts']['page_boxes']['page_top_a_column_width'] = array(
        '#type' => 'select',
        '#title' => t('Page Top A Columns'),
        '#description' => t('How many columns to show.'),
        '#default_value' => theme_get_setting('page_top_a_column_width'),
        '#options' => array(
            'uk-width-medium-1-1' => t('1 column 100%'),
            'uk-width-medium-1-2' => t('2 columns 50%'),
            'uk-width-medium-1-3' => t('3 columns 33%'),
            'uk-width-medium-1-4' => t('4 columns 25%'),
            'uk-width-medium-1-5' => t('5 columns 20%'),
            'uk-width-medium-1-6' => t('6 columns 16.6%'),

        ),
    );
    $form['layouts']['page_boxes']['page_top_a_box_style'] = array(
        '#type' => 'select',
        '#title' => t('Page Top A Panel Style'),
        '#default_value' => theme_get_setting('page_top_a_box_style'),
        '#options' => array(
            'uk-panel uk-panel-box' => t('Box'),
            'uk-panel uk-panel-box uk-panel-box-primary' => t('Box Primiry'),
            'uk-panel uk-panel-box uk-panel-box-secondary' => t('Box Secondary'),
            'uk-panel uk-panel-blank' => t('Box Blank'),
        ),

    );


    $form['layouts']['page_boxes']['page_top_b_column_width'] = array(
        '#type' => 'select',
        '#title' => t('Page Top B Columns'),
        '#description' => t('How many columns to show. '),
        '#default_value' => theme_get_setting('page_top_b_column_width'),
        '#options' => array(
            'uk-width-medium-1-1' => t('1 column 100%'),
            'uk-width-medium-1-2' => t('2 columns 50%'),
            'uk-width-medium-1-3' => t('3 columns 33%'),
            'uk-width-medium-1-4' => t('4 columns 25%'),
            'uk-width-medium-1-5' => t('5 columns 20%'),
            'uk-width-medium-1-6' => t('6 columns 16.6%'),

        ),
    );
    $form['layouts']['page_boxes']['page_top_b_box_style'] = array(
        '#type' => 'select',
        '#title' => t('Page Top B Boxes Panel Style'),
        '#default_value' => theme_get_setting('page_top_b_box_style'),
        '#options' => array(
            'uk-panel uk-panel-box' => t('Box'),
            'uk-panel uk-panel-box uk-panel-box-primary' => t('Box Primiry'),
            'uk-panel uk-panel-box uk-panel-box-secondary' => t('Box Secondary'),
            'uk-panel uk-panel-blank' => t('Box Blank'),
        ),

    );
// Page Bottom-----------------

    $form['layouts']['page_boxes']['page_bottom_a_column_width'] = array(
        '#type' => 'select',
        '#title' => t('Page Bottom A Columns'),
        '#description' => t('How many columns to show. '),
        '#default_value' => theme_get_setting('page_bottom_a_column_width'),
        '#options' => array(
            'uk-width-medium-1-1' => t('1 column 100%'),
            'uk-width-medium-1-2' => t('2 columns 50%'),
            'uk-width-medium-1-3' => t('3 columns 33%'),
            'uk-width-medium-1-4' => t('4 columns 25%'),
            'uk-width-medium-1-5' => t('5 columns 20%'),
            'uk-width-medium-1-6' => t('6 columns 16.6%'),

        ),


    );

    $form['layouts']['page_boxes']['page_bottom_a_box_style'] = array(
        '#type' => 'select',
        '#title' => t('Page Bottom A Boxes Panel Style'),
        '#default_value' => theme_get_setting('page_bottom_a_box_style'),
        '#options' => array(
            'uk-panel uk-panel-box' => t('Box'),
            'uk-panel uk-panel-box uk-panel-box-primary' => t('Box Primiry'),
            'uk-panel uk-panel-box uk-panel-box-secondary' => t('Box Secondary'),
            'uk-panel uk-panel-blank' => t('Box Blank'),
        ),

    );

    $form['layouts']['page_boxes']['page_bottom_b_column_width'] = array(
        '#type' => 'select',
        '#title' => t('Page Bottom B Columns'),
        '#description' => t('How many columns to show. '),
        '#default_value' => theme_get_setting('page_bottom_b_column_width'),
        '#options' => array(
            'uk-width-medium-1-1' => t('1 column 100%'),
            'uk-width-medium-1-2' => t('2 columns 50%'),
            'uk-width-medium-1-3' => t('3 columns 33%'),
            'uk-width-medium-1-4' => t('4 columns 25%'),
            'uk-width-medium-1-5' => t('5 columns 20%'),
            'uk-width-medium-1-6' => t('6 columns 16.6%'),

        ),


    );

    $form['layouts']['page_boxes']['page_bottom_b_box_style'] = array(
        '#type' => 'select',
        '#title' => t('Page Bottom B Panel Style'),
        '#default_value' => theme_get_setting('page_bottom_b_box_style'),
        '#options' => array(
            'uk-panel uk-panel-box' => t('Box'),
            'uk-panel uk-panel-box uk-panel-box-primary' => t('Box Primiry'),
            'uk-panel uk-panel-box uk-panel-box-secondary' => t('Box Secondary'),
            'uk-panel uk-panel-blank' => t('Box Blank'),
        ),

    );

// footer -----------------

    $form['layouts']['footer'] = array(
        '#type' => 'fieldset',
        '#title' => t(' Footers'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );


// Footer Middle-----------------

    $form['layouts']['footer']['footer_middle_column_width'] = array(
        '#type' => 'select',
        '#title' => t('Middle Columns'),
        '#description' => t('How many columns to show. '),
        '#default_value' => theme_get_setting('footer_middle_column_width'),
        '#options' => array(
            'uk-width-medium-1-1' => t('1 column 100%'),
            'uk-width-medium-1-2' => t('2 columns 50%'),
            'uk-width-medium-1-3' => t('3 columns 33%'),
            'uk-width-medium-1-4' => t('4 columns 25%'),
            'uk-width-medium-1-5' => t('5 columns 20%'),
            'uk-width-medium-1-6' => t('6 columns 16.6%'),

        ),
    );



// Footer Bottom-----------------


    $form['layouts']['footer']['no_branding_display'] = array(
        '#type' => 'checkbox',
        '#title' => t('Hide Orion branding'),
        '#default_value' => theme_get_setting('no_branding_display'),
        '#description'   => t("You can check this option to hide our branding. But please keep it uncheck. We would be grateful if you leave our tiny back link."),

    );
    $form['layouts']['footer']['didplay_copyright'] = array(
        '#type' => 'textfield',
        '#title' => 'Copyright',
        '#default_value' => theme_get_setting('didplay_copyright'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description'   => t("Enter text to display copyright at footer bottom. Leave blank to hide."),
    );

// CSS Customizer -----------------
    $form['css_edit']= array(
        '#type' => 'fieldset',
        '#title' => t('CSS Customizer(beta)'),
        '#group' => 'rs_orion',
    );

    $form['css_edit']['css_pre'] = array(
        '#type' => 'item',
        '#markup' => t('<br /><strong>Important note:</strong><br />
        We work on our theme yet and do not use all element, we keep it just to show power of framework and add them if feature.<br />
        After starting Customizer<strong> wait a little</strong>, it can take couple of minutes to build temp css, refresh if not loading long
        <br />
        <img src="' . file_create_url(drupal_get_path('theme', 'rs_orion') . '/images/customizer.png') . '" />
        '),
    );

    $form['css_edit']['css_howto'] = array(
        '#type' => 'item',
       '#title' => t('Heading'),
        '#markup' => t('For security reasons, we disable the direct CSS saving, we are going to do it in the nearest theme update.
        <br /><strong>How to use.</strong><br />
        1. Open Customizer<br />
        2. Make necessary changes<br />
        3. Click get CSS<br />
        4. If you customize theme on the local server save as ukit.css in <strong>YOUR_local_site_direction/sites/all/themes/rs_orion/css/</strong><br />
          &nbsp; &nbsp; If you are editing on a remote server - save on local machine and then upload via ftp to <strong>YOURSITE/sites/all/themes/rs_orion/css/</strong><br />
        <strong>Note:</strong> Customizer does not save LESS file, if you want to save your changes to continue later, save LESS as well, you can upload it later.<br />
        <br />
        <div align="center"><a href="' . file_create_url(drupal_get_path('theme', 'rs_orion') . '/docs/customizer.html') . '" style="font-size: 24px" target="_blank"><br /><strong>OPEN CUSTOMIZER</strong></a></div>
        <div align="center"><br /><strong>As LESS files require much space we did not include alternate source files  into theme package, you can download latest packages from <a href="http://orion.ready-site.ru"  target="_blank">project page</a>.</strong></div>'),
            );



// Help and Documentation -----------------
    $form['info']= array(
        '#type' => 'fieldset',
        '#title' => t('Help'),
        '#group' => 'rs_orion',
    );

    $form['info']['info_overview'] = array(
        '#type' => 'item',
        '#title' => t('Info'),
        '#markup' => t('
<strong>This section will be updated in next releases. <br /> For more information visit <a href="http://orion.ready-site.ru"  target="_blank">Orion homepage</a></strong><br />

'),
    );

    $form['info']['lyout_picture'] = array(
        '#markup' => t(''),
        '#prefix' => '<img src="' . file_create_url(drupal_get_path('theme', 'rs_orion') . '/images/layouts.png') . '" /><br />',
    );



// Collapse default forms

    $form['theme_settings']['#collapsible'] = TRUE;
    $form['theme_settings']['#collapsed'] = TRUE;
    $form['logo']['#collapsible'] = TRUE;
    $form['logo']['#collapsed'] = TRUE;
    $form['favicon']['#collapsible'] = TRUE;
    $form['favicon']['#collapsed'] = TRUE;

}
