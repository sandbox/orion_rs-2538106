<?php





// Override main menu class
function rs_orion_menu_tree__main_menu($variables) {
    return '<ul id="rs-nav" class="use-trans">' . $variables['tree'] . '</ul>';
}

//Override main menu submenu class
function rs_orion_menu_link(array $variables) {
    $element = $variables['element'];
    $sub_menu = '';

    if ($element['#below']) {
        // Wrap in dropdown-menu.
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '<div class="submenu"><ul>' . drupal_render($element['#below']) . '</ul></div>';
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}



