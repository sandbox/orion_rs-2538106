<?php
    $page_top_a_panel_wrap = check_plain(theme_get_setting('page_top_a_box_style'));
    $page_top_a_column_width = check_plain(theme_get_setting('page_top_a_column_width'));
?>



    <div class="<?php echo $page_top_a_column_width; ?>"><div class="<?php echo $page_top_a_panel_wrap; ?> <?php print $classes; ?>" <?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
        <h3 class="uk-panel-title" <?php print $title_attributes; ?>><?php print $block->subject ?></h3>
    <?php endif;?>
    <?php print render($title_suffix); ?>


        <?php print $content ?>

        </div></div>
