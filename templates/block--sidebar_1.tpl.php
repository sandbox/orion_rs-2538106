<?php
    $sidebar_1_panel_wrap = check_plain(theme_get_setting('sidebar_1_style'));
?>



    <div id="<?php print $block_html_id; ?>" class="<?php echo $sidebar_1_panel_wrap; ?> <?php print $classes; ?>" <?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
        <h3 class="uk-panel-title" <?php print $title_attributes; ?>><?php print $block->subject ?></h3>
    <?php endif;?>
    <?php print render($title_suffix); ?>


        <?php print $content ?>

        </div>
