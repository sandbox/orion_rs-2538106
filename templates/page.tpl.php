
<?php
include_once('vars.php');
include_once('header.tpl.php');

?>


<?php if ($is_front && theme_get_setting('display_parallax_setting')): ?>

<div class="uk-text-contrast uk-text-center uk-hidden-small uk-flex uk-flex-center uk-flex-middle"  data-uk-parallax="{bgp: '50'}"" style="height: 450px; background-image: url('<?php echo  $top_parallax_bg_url ?>'); background-position: top center;">
    <div class="uk-width-medium-1-2 uk-parallax" data-uk-parallax="{ scale: '1.8'}">
        <h1 <?php echo $parallax_wrap; ?>><?php echo $head_parallax_setting; ?></h1>
       <p <?php echo $parallax_wrap; ?>><a href="<?php echo $url_parallax_setting; ?>" class="uk-text-large"><?php echo $desc_parallax_setting; ?></a></p>
    <?php if (theme_get_setting('display_button_parallax_setting')): ?><div class="uk-button uk-button-primary"><a href="<?php echo $url_parallax_setting; ?>" class="uk-text-large"><?php echo $read_more_parallax_setting; ?></a></div><?php endif; ?>
    </div>
</div>


<?php endif; ?>

<?php if ($page['slideshow']): ?>
    <div <?php echo $slideshow_container; ?> class="uk-slideshow uk-hidden-small">

        <?php print render($page['slideshow']); ?>
    </div>
<?php endif; ?>

<?php echo $container_open_div; ?>

            <?php if ($page['top_a']): ?>
                <div class="uk-grid uk-grid-medium uk-page-top">
                    <?php print render($page['top_a']); ?>
                </div>
        <?php endif; ?>
            <?php if ($page['top_b']): ?>
                <div class="uk-grid uk-grid-medium uk-page-top">
                    <?php print render($page['top_b']); ?>
                </div>
        <?php endif; ?>

<?php echo $wrap_content_container_open_div; ?>

        <div class="uk-grid uk-grid-medium uk-content">

        <div class="tm-main  <?php echo $contentwidth; ?> <?php echo $content_pp; ?>">

                <article class="uk-article">


    <a id="main-content"></a>
                            <?php print render($title_prefix); ?>
<?php
 if (drupal_is_front_page()) { $variables['title']=""; } 
else{
                            
echo '<h1 class="title" id="page-title">';
echo  $title;
echo  '</h1>';
}
?>
                      
                            <?php print render($title_suffix); ?>
                            <?php if ($tabs): ?>
                                <div class="tabs">
                                    <?php print render($tabs); ?>
                                </div>
                            <?php endif; ?>
                            <?php print render($page['help']); ?>
                            <?php if ($action_links): ?>
                                <ul class="action-links">
                                    <?php print render($action_links); ?>
                                </ul>
                            <?php endif; ?>
                    <?php  if(drupal_is_front_page())
                    {
                        unset($page['content']['system_main']['default_message']);
                    }
                    print render($page['content']);
                    ?>

                            <?php print $feed_icons; ?>

                </article>

            </div>

        <?php if ($page['sidebar_1'] or $page['sidebar_2']): ?>


            <div class="tm-sidebar uk-width-medium-1-4 <?php echo $sidebar_pp; ?>">
                <?php if ($page['sidebar_1']): ?>
                  <?php print render($page['sidebar_1']); ?>
                <?php endif; ?>
                <?php if ($page['sidebar_2']): ?>
                    <?php print render($page['sidebar_2']); ?>
                <?php endif; ?>

            </div>
        <?php endif; ?>

    </div>
<?php echo $wrap_content_container_close_div; ?>
            <?php if ($page['bottom_a']): ?>
                <div class="uk-grid uk-grid-medium uk-page-bottom">
                    <?php print render($page['bottom_a']); ?>
                </div>
            <?php endif; ?>
            <?php if ($page['bottom_b']): ?>
                <div class="uk-grid uk-grid-medium uk-page-bottom">
                    <?php print render($page['bottom_b']); ?>
                </div>
            <?php endif; ?>
            <?php echo $container_close_div; ?>


<footer class="tm-footer">
    <?php if ($page['footer_top']): ?>
        <div class="footer-top">
            <?php echo $container_open_div; ?>

                <?php print render($page['footer_top']); ?>

                <?php echo $container_close_div; ?>
        </div>
    <?php endif; ?>
    <?php if ($page['footer_middle']): ?>
        <div class="footer-middle">
            <?php echo $container_open_div; ?>
            <div class="uk-grid uk-grid-medium">
            <?php print render($page['footer_middle']); ?>
  </div>
            <?php echo $container_close_div; ?>
        </div>
<?php endif; ?>

            <?php if ($page['footer_bottom'] or  $didplay_copyright): ?>

                <div class="footer-bottom">
                    <?php echo $container_open_div; ?>
                        <?php print render($page['footer_bottom']); ?>
                     <?php echo $container_close_div; ?>
                    <?php if (theme_get_setting('didplay_copyright')): ?> <div class="rs-copyright"><i class="uk-icon-hover uk-icon-copyright"></i> <?php print $didplay_copyright; ?></div><?php endif; ?>

                </div>

            <?php endif; ?>

    <?php if ($branding = 'show'): ?>
    <div class="rs-branding">
                   <a href="http://orion.ready-site.ru">Powered by Orion RS</a>
            </div>
    <?php endif; ?>

</footer>

<?php echo $wrap_container_close_div; ?>

<div id="tm-offcanvas" class="uk-offcanvas">

    <div class="uk-offcanvas-bar">
<div id="rs-offcanvas">

    <?php print theme('links__system_main_menu', array(
        'links' => $main_menu,
        'attributes' => array(
            'id' => 'main-menu-links',
            'id' => 'submenu',
            'class' => array('links', 'clearfix'),
        ),

    )); ?>

    <?php print theme('links', array(
        'links' => $secondary_menu,
        'attributes' => array(
            'id' => 'submenu',
            'class' => array('links', 'clearfix'),
        ),
        'heading' => array(
            'text' => t('Secondary menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
        ),
    )); ?>
    <div class="offcanvas-search">
           <?php
        $block = module_invoke('search', 'block_view');
        print render($block['content']);
        ?>
</div>

</div>
    </div>

</div>


