<?php
$footer_middle_width = check_plain(theme_get_setting('footer_middle_column_width'));
$footer_middle_panel_wrap = check_plain(theme_get_setting('footer_box_style'));
?>



    <div class="<?php echo $footer_middle_width; ?>"><div id="<?php print $block_html_id; ?>" class="<?php echo $footer_middle_panel_wrap; ?> <?php print $classes; ?>" <?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
        <h3 class="uk-panel-title" <?php print $title_attributes; ?>><?php print $block->subject ?></h3>
    <?php endif;?>
    <?php print render($title_suffix); ?>


        <?php print $content ?>

        </div></div>
