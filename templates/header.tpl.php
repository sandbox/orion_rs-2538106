<?php

include_once('vars.php');
$search_display = theme_get_setting('search_display');
$menu_float = theme_get_setting('menu_float');
$header_height = theme_get_setting('header_height');
?>


<?php echo $wrap_container_open_div; ?>
<?php if (theme_get_setting('classic_style')): ?>
    <!-- classic header   -->

    <?php if (theme_get_setting('site_contacts', 'rs_orion')): ?>

        <div class="tm-toolbar uk-clearfix uk-hidden-small">
            <div class="uk-container uk-container-center">


                <div class="uk-float-left">



                    <?php if ($facebook_url): ?>

                        <a href="<?php print $facebook_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-facebook"></i>
                        </a>

                    <?php endif; ?>
                    <?php if ($vk_url): ?>

                        <a href="<?php print $vk_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-vk"></i>
                        </a>

                    <?php endif; ?>
                    <?php if ($twitter_url): ?>

                        <a href="<?php print $twitter_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-twitter"></i>
                        </a>

                    <?php endif; ?>
                    <?php if ($google_url): ?>

                        <a href="<?php print $google_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-google-plus"></i>
                        </a>

                    <?php endif; ?>

                </div>

                <div class="uk-float-right">
                    <?php if ($company_phone): ?>
                        <i class="uk-icon-hover uk-icon-phone-square"></i> <?php print($company_phone); ?>
                    <?php endif; ?>
                    <?php if ($company_email): ?>
                        <a href="mailto:<?php print($company_email); ?>"> <i class="uk-icon-hover uk-icon-envelope"></i><?php print($company_email); ?></a>
                    <?php endif; ?>
                </div>

            </div>

        </div>
    <?php endif; ?>

    <header class="tm-header">
        <div class="uk-container uk-container-center">
            <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                </a>
            <?php endif; ?>
            <?php if ($site_name): ?>
                <div class="name-and-slogan">

                    <?php if ($site_name): ?>
                        <?php if ($title): ?>


                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" ><?php print $site_name; ?></a>

                        <?php endif; ?>
                    <?php endif; ?>



                </div> <!-- /#name-and-slogan -->
            <?php endif; ?>
            <?php if ($search_display == 'show'): ?>

                <div class="uk-search uk-flex uk-flex-middle uk-hidden-small"  style="height: <?php echo $header_height; ?>">
                    <?php
                    $block = module_invoke('search', 'block_view');
                    print render($block['content']);
                    ?>
                </div>

            <?php endif; ?>
            <?php if ($page['header_a']): ?>
                <div class="rs-header-flex uk-width-medium-1-3"  style="height: <?php echo $header_height; ?>">

                    <?php print render($page['header_a']); ?>

                </div>

            <?php endif; ?>

        </div>
    </header>


    <nav class="tm-navbar uk-navbar" <?php

    switch ($navbar) {
        case "sticky":
            echo "data-uk-sticky";
            break;
        case "sticky2":
            echo 'data-uk-sticky="{top:-200, animation: \'uk-animation-slide-top\'}"';
            break;

    }?>>
        <div class="uk-container uk-container-center">


            <div class="mainmenu  uk-hidden-small <?php echo $menu_float; ?> ">

                <?php $menu = menu_tree('main-menu');
                echo render($menu);
                ?>

            </div>

            <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

            <div class="uk-navbar-brand uk-navbar-center uk-visible-small">
            </div>
        </div>

    </nav>


<!-- normall header   -->
    <?php else: ?>
    <?php if (theme_get_setting('site_contacts', 'rs_orion')): ?>

        <div class="tm-toolbar uk-clearfix uk-hidden-small">
            <?php echo $container_open_div; ?>


                <div class="uk-float-left">



                    <?php if ($facebook_url): ?>

                        <a href="<?php print $facebook_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-facebook"></i>
                        </a>

                    <?php endif; ?>
                    <?php if ($vk_url): ?>

                        <a href="<?php print $vk_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-vk"></i>
                        </a>

                    <?php endif; ?>
                    <?php if ($twitter_url): ?>

                        <a href="<?php print $twitter_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-twitter"></i>
                        </a>

                    <?php endif; ?>
                    <?php if ($google_url): ?>

                        <a href="<?php print $google_url; ?>" target="_blank">
                            <i class="uk-icon-hover uk-icon-google-plus"></i>
                        </a>

                    <?php endif; ?>

                </div>

                <div class="uk-float-right">
                    <?php if ($company_phone): ?>
                        <i class="uk-icon-hover uk-icon-phone-square"></i> <?php print($company_phone); ?>
                    <?php endif; ?>
                    <?php if ($company_email): ?>
                        <a href="mailto:<?php print($company_email); ?>"> <i class="uk-icon-hover uk-icon-envelope"></i><?php print($company_email); ?></a>
                    <?php endif; ?>
                </div>

                <?php echo $container_close_div; ?>

        </div>
    <?php endif; ?>

<nav class="tm-navbar uk-navbar" <?php

switch ($navbar) {
    case "sticky":
        echo "data-uk-sticky";
        break;
    case "sticky2":
        echo 'data-uk-sticky="{top:-200, animation: \'uk-animation-slide-top\'}"';
        break;

}?>>
    <?php echo $container_open_div; ?>
        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
        <?php endif; ?>
        <?php if ($site_name): ?>
            <div class="name-and-slogan">

                <?php if ($site_name): ?>
                    <?php if ($title): ?>


                        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" ><?php print $site_name; ?></a>

                    <?php endif; ?>
                <?php endif; ?>

            </div> <!-- /#name-and-slogan -->
        <?php endif; ?>

        <?php if ($search_display == 'show'): ?>

            <div class="uk-search uk-flex uk-flex-middle uk-hidden-small"  style="height: <?php echo $header_height; ?>">
                <?php
                $block = module_invoke('search', 'block_view');
                print render($block['content']);
                ?>
            </div>

        <?php endif; ?>

        <?php if ($main_menu): ?>
            <div  id="menu-trigger" class="uk-hidden-small <?php echo $menu_float; ?> uk-flex uk-flex-middle"  style="height: <?php echo $header_height; ?>">

                <?php $menu = menu_tree('main-menu');
                echo  render($menu);
                ?>
            </div>
        <?php endif; ?>



<div class="offconvas-button">
    <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
</div>
        <?php echo $container_close_div; ?>

</nav>

<?php endif; ?>



