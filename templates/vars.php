

<?php

// social and contact -----------------
$facebook_url = check_plain(theme_get_setting('facebook_url', 'rs_orion'));
$vk_url = check_plain(theme_get_setting('vk_url', 'rs_orion'));
$twitter_url = check_plain(theme_get_setting('twitter_url', 'rs_orion'));
$google_url = check_plain(theme_get_setting('google_url', 'rs_orion'));
$company_phone = check_plain(theme_get_setting('company_phone', 'rs_orion'));
$company_email = check_plain(theme_get_setting('company_email', 'rs_orion'));
$theme_path_social = base_path() . drupal_get_path('theme', 'rodoladnew');
$didplay_copyright = check_plain(theme_get_setting('didplay_copyright'));
$header_region_float = check_plain(theme_get_setting('header_region_float'));


//-----------------
$sidebar_pozition = check_plain(theme_get_setting('rs_orion_sidebar_position'));

// boxed layouts -----------------
$page_width = check_plain(theme_get_setting('page_layouts'));

if($page_width == 1){
    $wrap_container_open_div = '<div class="uk-container uk-container-center"><div class="rs-container">';
    $wrap_container_close_div = '</div></div>';
    $container_open_div = '<div class="rs-paddind">';
    $container_close_div = '</div>';
}
elseif ($page_width == 2){
    $wrap_container_open_div = '';
    $wrap_container_close_div = '';
    $container_open_div = '<div class="rs-paddind">';
    $container_close_div = '</div>';
}
else{
    $wrap_container_open_div = '';
    $wrap_container_close_div = '';
    $container_open_div = '<div class="uk-container uk-container-center">';
    $container_close_div = '</div>';
}

// content conainer -----------------
if(theme_get_setting('contant_container')){
    $wrap_content_container_open_div = '<div class="rs-content-container">';
    $wrap_content_container_close_div = '</div>';

}
else{
    $wrap_content_container_open_div = '';
    $wrap_content_container_close_div = '';
}
//  nav bar -----------------
$navbar = check_plain(theme_get_setting('rs_orion_navbar_position'));


// parallax varables -----------------
$head_parallax_setting = theme_get_setting('head_parallax_setting');
$desc_parallax_setting = theme_get_setting('desc_parallax_setting');
$read_more_parallax_setting = theme_get_setting('read_more_parallax_setting');
$url_parallax_setting = theme_get_setting('url_parallax_setting');
$parallax_wrap = theme_get_setting('wrap_text_parallax_setting');
$top_parallax_bg_url_exist = theme_get_setting('top_parallax_bg_url');

if(theme_get_setting('top_parallax_bg_url')){
    $top_parallax_bg_url = $top_parallax_bg_url_exist;

}
else{
    $top_parallax_bg_url = base_path() . drupal_get_path('theme', 'rs_orion') . '/images/parallax-bg.jpg';
}
if($sidebar_pozition == 'sidebar_left'){
    $content_pp ='uk-push-1-4';
    $sidebar_pp ='uk-pull-3-4';
}
else{
    $content_pp ='';
    $sidebar_pp ='';
}
// slideshow container -----------------
if(theme_get_setting('show_slideshow_container')){
    $slideshow_container ='class="uk-container uk-container-center"';
}
else{
    $slideshow_container ='';
}
// content width -----------------
if ($page['sidebar_1'] or $page['sidebar_2']){
    $contentwidth ='uk-width-medium-3-4';

}
else{
    $contentwidth ='uk-width-1-1';
}
// sidebar position -----------------
if($sidebar_pozition == 'sidebar_left'){
    $content_pp ='uk-push-1-4';
    $sidebar_pp ='uk-pull-3-4';
}
else{
}
// parallax -----------------
if(theme_get_setting('wrap_text_parallax_setting')){
    $parallax_wrap = 'class="wrap"';
}
else{
    $parallax_wrap = '';
}
// branding -----------------
if(theme_get_setting('no_branding_display')){
    $branding = '';
}
else{
    $branding = 'wrap';
}


?>


