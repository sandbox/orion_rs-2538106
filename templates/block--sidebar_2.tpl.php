<?php
    $sidebar_2_panel_wrap = check_plain(theme_get_setting('sidebar_2_style'));
?>



    <div id="<?php print $block_html_id; ?>" class="<?php echo $sidebar_2_panel_wrap; ?> <?php print $classes; ?>" <?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
        <h3 class="uk-panel-title" <?php print $title_attributes; ?>><?php print $block->subject ?></h3>
    <?php endif;?>
    <?php print render($title_suffix); ?>

    <div class="content"<?php print $content_attributes; ?>>
        <?php print $content ?>
    </div>
        </div>
